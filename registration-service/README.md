registration-service
--------------------

The Registration Service handles registration requests.

## Getting Started

To clone the repo:

```shell
git clone git@gitlab.com/dctx/devcon-contact-tracer.git
cd registration-service
```

A docker image is also avaible in Docker Hub:

```shell
docker pull dctx/registration-service:latest
```

## Prerequisire

### Workspace

- Go
- Docker

### Docker images

- Kafka (bitnami/kafka:2.4.1)

### Deployment

- Docker Compose
- Kubernetes (minikube)
- Helm

## Build

Run the following to build the binary:

```shell
make build
```

The binary will be located in: `build/bin/registration-service`

## Configuration

Configuration is handled by [viper](https://github.com/spf13/viper) which allows configuration using a config file or by environment variables.

A sample configuration file can be found in `config/.registration-service.yaml`. Copy this to the `$HOME` directory to override the defaults.

Setting environment variables can also override the default configuration:

| Environment Variable | Description | Default |
| -------------------- | ----------- | ------- |
|                      |             |         |


## Starting the Service

```shell
make run
```

## Packaging Image

To create the docker image:

```
make package
```

To publish the image to docker hub:

```
make publish
```

Note that publishing the image requires access to the dctx group in docker hub.

## Development

This project follows gitlab flow. The general flow is:

- Pick / Create an issue
- Create a feature branch
- Open a Merge Request
- Maintainer merge's to `master`

Read more about gitlab flow [here](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

### Project Structure

```
registration-service
|- build/               # build artifacts are generated here
|- cmd/                 # command line commands live here. Checkout cobra library
|- config/              # configuration files are here
|- db/                  # for database migration files
|- helm/                # helm chart for kubernetes deployment
|- internal/            # for internal go packages 
| |- server
| |- ...
|- pkg/                 # for public go packages
|- .dockerignore        # ignore list for docker
|- .gitignore           # ignore list for git
|- go.mod               # dependencies for project
|- go.sum               # checksum for dependencies, do not manually change
|- main.go              # the main go file
|- Makefile             # build scripts
|- README.md            # this file
```

### Adding Dependencies

To add dependencies, run the following:

```shell
go get -u {dependency}
make deps
```

## TODO

Initial ToDo list:

- [ ] Improve this README
- [ ] Setup Gitlab CI/CD
- [ ] Create kafka connector
- [ ] Define features in Gitlab Issues or Trello