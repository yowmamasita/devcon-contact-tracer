package server

import "time"

// Run runs the server and it's services
func Run() error {

	// TODO, replace with something that can be cancelled. Should also start listening to Kafka
	for {
		time.Sleep(1 * time.Second)
	}
	return nil
}
