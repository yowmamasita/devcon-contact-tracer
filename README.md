DevCon Contact Tracer
---------------------

...

## Getting Started

The clone the repo:

```shell
git clone git@gitlab.com/dctx/devcon-contact-tracer.git
```

## Components

- [graph-service](./graph-service/README.md)
- [registration-service](./registration-service/README.md)

## TODO

Initial ToDo list:

- [ ] Improve this README
