package cmd

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/dctx/devcon-contact-tracer/graph-service/internal/migration"
)

// migrateCmd represents the migrate command
var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "run migrate",
	Run:   runMigrate,
}

func init() {
	rootCmd.AddCommand(migrateCmd)
}

func runMigrate(c *cobra.Command, args []string) {
	log.Info("Starting Migration")
	if err := migration.Run(); err != nil {
		log.WithError(err).Error("error occured when running migrations")
		os.Exit(1)
	}
	log.Info("Migration complete")
}
