package main

import "gitlab.com/dctx/devcon-contact-tracer/graph-service/cmd"

var version = "dev"

func main() {
	cmd.Execute(version)
}
